# -*- coding: utf-8 -*-
'''
@file fibonacci.py
'''

def fib(idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    # first create a list containing the first two numbers of the Fibonacci sequence
    seq = [0,1]
    # since indices of first two numbers happen to be the first two numbers of sequence, use these
    if idx == 0 or idx == 1:
        return seq[idx]
    else:
        # first create list of all indices from 2 to the selected index
        count = range(2,idx+1)
        # use a for loop to calculate the Fibonacci number at each of these indices
        for n in count:
            seq.append(seq[n-1]+seq[n-2]) # appends the calculated number to the list of Fib numbers
        return seq[idx] # the number at the index selected by the user is the corresponding Fib number
            
if __name__ == '__main__':
    #create an infinite loop so that user is continually prompted for an index
    while True:
        user = input('Please enter a Fibonacci Index: ') # user's input
        if user.isnumeric() == True: # Handles valid indices
            idx = int(user) # converts input to an integer (as all inputs start as strings)
            print('Fibonacci Number at index {:} is {:}.'.format(idx,fib(idx))) # displays Fib number
            # Now give user the choice of entering another index or exiting
            choice = input('Press ENTER to input another index or press q to exit: ')
            if choice == 'q': # entering q breaks the infinite loop, closing the program
                break
        else: # Handles invalid indices and allows user to try again, telling them what is valid
            print('Invalid index.  A valid index is any integer greater '
                'than or equal to zero.  Please try again.')