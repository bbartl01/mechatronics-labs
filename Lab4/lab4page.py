'''! @page lab4page Lab4
    @tableofcontents
    Closed Loop Speed Control
    
    @section sec_intro4 Introduction
    This lab expanded upon Lab 3 by adding the ability to control the speed of the motor
    via closed loop control (as opposed to open loop).  In other words, instead of 
    having to input a duty cycle, the user could simply input the desired speed of the
    motor to get the motor rotate at that speed.  This program made use of integral control,
    helping drastically minimize error in speed.  In addition to being able to select the setpoint, 
    or the speed of the motor, this program enabled the user to select the Kp and Ki values
    used to computed the required actuation, or duty cycle to achieve the desired velocity.  
    
    A block diagram depicting the structure of our closed loop control is depicted below:
    @image html BlockDiagram4.png width=1000px
    
    One key feature of this updated program is the ability to perform a step response
    test on the motor to determine the effect that the user's selected values for Kp and 
    Ki had on speed control.  Below are three plots depicting the level of control for
    various Kp's and Ki's:
    
    Plot 1:
    @image html Lab4Data1.png width=500px
    
    Plot 2:
    @image html Lab4Data2.png width=500px
    
    Plot 3:
    @image html Lab4Data3.png width=500px
    
    The first plot establishes a baseline for Kp and Ki for which to disucss the effect
    of modifications of the two.  As can be seen, while steady state error in speed is 
    low and oscillation amplitudes are kept to a minimum, the settling time leaves more 
    to be desired.  
    
    The second plot holds Ki constant from the first plot, but increases Kp.  As can be 
    seen, this reduces the settling time by roughly 50% and also slightly reduces the 
    steady state error.  However, this modification results in a heightened oscillation
    before the motor reaches steady state.  As seen, the oscillation is also present in 
    the actuation signal.  Such oscillations should be avoided as they can break the motor.  
    
    The last plot holds Kp constant from the first plot but increases.  This has the effect 
    of reducing the settling time even further (a 75% reduction in this case) and keeps 
    oscillations to a minimum.  The only perceived drawback is that the steady state error
    is barely modified, but considering that this error is already minimal, this is an 
    acceptable trade off.  
    
    @section sec_files4 Files
    
    @subsection subsec_main4 main
    This file calls all the tasks and classes necessary for the interface to work.
    See @ref main.py for more information.  Task diagram is depicted below:
    @image html taskDiagram4.png width=500px
    
    @subsection subsec_UTask4 User Task
    This file creates the task that runs the User Interface.
    Please see @ref UTask.py for more information.  State transition diagram is
    depicted below:
    @image html UTaskSTD4.png width=1000px
    
    @subsection subsec_CTask4 Conroller Task
    This file creates the task that performs closed loop control on a motor.  
    Please see @ref CTask.py for more information.  State transition diagram is
    depicted below:
    @image html CTaskSTD4.png width=500px
    
    @subsection subsec_ETask4 Encoder Task
    This file creates the task that collects data from the encoder.  
    Please see @ref ETask.py for more information.  State transition diagram is
    depicted below:
    @image html ETaskSTD3.png width=500px
    
    @subsection subsec_MTask4 Motor Task
    This file creates the task that changes the duty cycles applied to the motors.  
    Please see @ref MTask.py for more information.  State transition diagram is 
    depicted below:
    @image html MTaskSTD3.png width=500px
    
    @subsection subsec_STask4 Safety Task
    This file creates the task that enables and disbales the motor.  
    Please see @ref STask.py for more information.  State transition diagram is 
    depicted below:
    @image html STaskSTD3.png width=500px

    @subsection subsec_enc4 Encoder Driver
    This file creates the encoder class.  
    Please see @ref encoder.py for details about the encoder driver.  
    Please see @ref encoder.Encoder for details about the encoder class.
    
    @subsection subsec_CLC4 Closed Loop Controller Class
    This file creates the class that enables closed loop control.  
    Please see @ref closedloop.ClosedLoop for details about this class.  
    Please see @ref closedloop.py for details about the file.
    
    @subsection subsec_mot4 motor Driver
    This file creates the motor class.  
    Please see @ref motor.py for details about the motor driver.  
    Please see @ref motor.Motor for details about the motor class.
    
    @subsection subsec_DRV4 DRV8847
    This file creates DRV8847, a class that controls enabling and disabling of the motors.   
    Please see @ref DRV8847.py for details about DRV8847.  
    Please see @ref DRV8847.DRV8847 for details about the DRV8847 class.
    
    @subsection subsec_shares4 Shares Class
    This file creates the class that enables variables to be shared across multiple tasks.  
    See @ref shares.py for more information.
'''