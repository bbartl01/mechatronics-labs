'''! @page lab5page Lab5
    @tableofcontents
    I2C and Inertial Measurement Units
    
    @section sec_intro5 Introduction
    This lab extends upon Lab 4 in a few ways.  First, closed loop control is now used to 
    control two motors simultaneously.  Second, these motors are used to control the 
    angle of a platform.  The goal is to make the platform level.  Third, we got practice 
    interfacing with an IMU.  This invloved setting up a calibration procedure as well as 
    the ability to read calibration coefficients to the IMU.  For this lab, we used the BNO055.  
    With the IMU, there was no longer a need for an encoder, as the IMU could obtain angular velocity.  
    Because of the difficulty of this lab, no virtual deliverables were required (we simply had to 
    show our instructor that the platform would level itself and that we could calibrate the IMU).  
    
    @section sec_files5 Files
    
    @subsection subsec_main5 main
    This file calls all the tasks and classes necessary for the interface to work.
    See @ref main.py for more information.  
    
    @subsection subsec_UTask5 User Task
    This file creates the task that runs the User Interface.
    Please see @ref UTask5.py for more information.
    
    @subsection subsec_IMUTask IMU Task
    This file creates the task that runs the IMU.
    Please see @ref IMUTask.py for more information.
    
    @subsection subsec_CTask5 Conroller Task
    This file creates the task that performs closed loop control on a motor.  
    Please see @ref CTask5.py for more information.
    
    @subsection subsec_MTask5 Motor Task
    This file creates the task that changes the duty cycles applied to the motors.  
    Please see @ref MTask.py for more information.
    
    @subsection subsec_BNO BNO055 Driver
    This file creates the driver for the BNO055. 
    Please see @ref BNO055.BNO055 for details about this class.  
    Please see @ref BNO055.py for details about the file.
    
    @subsection subsec_CLC5 Closed Loop Controller Class
    This file creates the class that enables closed loop control.  
    Please see @ref closedloop5.ClosedLoop for details about this class.  
    Please see @ref closedloop5.py for details about the file.
    
    @subsection subsec_mot5 motor Driver
    This file creates the motor class.  
    Please see @ref motor5.Motor for details about the motor class.
    
    @subsection subsec_shares5 Shares Class
    This file creates the class that enables variables to be shared across multiple tasks.  
    See @ref shares.py for more information.
'''