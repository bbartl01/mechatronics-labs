'''! @page lab3page Lab3
    @tableofcontents
    DC Motor Control
    
    @section sec_intro3 Introduction
    This lab extended the User Interface from Lab 2 to incorporate control of two motors.  
    The motors were controlled by via pulse width modulation.  Just as before, 
    the user could also use the encoder to collect data describing the position of the
    motor over a set period of time.  An example of such data is seen below:
    @image html Lab3Data1.png
    Another functionality was added where the user could select duty cycles of motor 1 
    and have the average velocity at that duty cycle computed.  A graph of some data 
    collected is seen below:
    @image html Lab3Data2.png width=600px
    
    @section sec_main3 main
    This file calls all the tasks and classes necessary for the interface to work.
    See @ref main.py for more information.  Task diagram is depicted below:
    @image html taskDiagram3.png width=500px
    
    @section sec_UTask3 User Task
    This file creates the task that runs the User Interface.
    Please see @ref UTask.py for more information.  State transition diagram is
    depicted below:
    @image html UTaskSTD3.png width=500px
    
    @section sec_ETask3 Encoder Task
    This file creates the task that collects data from the encoder.  
    Please see @ref ETask.py for more information.  State transition diagram is
    depicted below:
    @image html ETaskSTD3.png width=500px
    
    @section sec_MTask3 Motor Task
    This file creates the task that changes the duty cycles applied to the motors.  
    Please see @ref MTask.py for more information.  State transition diagram is 
    depicted below:
    @image html MTaskSTD3.png width=500px
    
    @section sec_STask3 Safety Task
    This file creates the task that enables and disbales the motor.  
    Please see @ref STask.py for more information.  State transition diagram is 
    depicted below:
    @image html STaskSTD3.png width=500px

    @section sec_enc3 Encoder Driver
    This file creates the encoder class.  
    Please see @ref encoder.py for details about the encoder driver.  
    Please see @ref encoder.Encoder for details about the encoder class.
    
    @section sec_mot3 motor Driver
    This file creates the motor class.  
    Please see @ref motor.py for details about the motor driver.  
    Please see @ref motor.Motor for details about the motor class.
    
    @section sec_DRV3 DRV8847
    This file creates DRV8847, a class that controls enabling and disabling of the motors.   
    Please see @ref DRV8847.py for details about DRV8847.  
    Please see @ref DRV8847.DRV8847 for details about the DRV8847 class.
    
    @section sec_shares3 Shares Class
    This file creates the class that enables variables to be shared across multiple tasks.  
    See @ref shares.py for more information.
'''