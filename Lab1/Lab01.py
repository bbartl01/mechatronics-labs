"""!
@file lab01.py
@brief This program runs a loop of three types of blinking pattern for a LED
@details This code allows users to have a Nucleo display one of three light
         patterns.  Upon bootup, the program starts off in a non-blinking state,
         instructing the user to press the blue button on the Nucleo.  Upon
         pressing the button, the LED on the Nucleo gives a square wave pattern.  
         Pressing the button again will switch to a Sine Wave patter.  Pressing 
         the button a third time will change the patter to a Saw Wave.  Continuing 
         to press the button will allow the user to cycle through these three patterns.

         @image html State1-1.png width=500px
         @image html State1-2.png width=500px
             
         See Source Code:
         https://bitbucket.org/bbartl01/mechatronics-labs/src/master/Lab1/Lab01.py
         See Short Demo Video Here:
         https://drive.google.com/file/d/1N51olmz1UC8SNU7ujMx1YvxXh7s-OVZv/view
         
@author Baxter Bartlett
@author Miles Ibarra
@date 01-20-22
"""

import time
import math
import pyb

## @brief Connect to LED
#  @details Creates a pin that enables control of the LED on the Nucleo
#
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

## @brief Creates timer
#  @details Creates a timer
#
tim2 = pyb.Timer (2, freq = 20000)

## @brief Controls PWM
#  @details Uses the timer to provide an interface for user to control the 
#           pulse width modulation.  Control over PWM enables the LED patterns.
#
t2ch1 = tim2.channel (1, pyb.Timer.PWM, pin=pinA5)

## @brief Connect to blue button
#  @details Creates a pin that enables program to receive inputs from the blue
#           button on the Nucleo
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)


def onButtonPressFCN(IRQ_src):
   '''! @brief Callback function tied to blue button on Nucleo
        @details The fucntion causes the variable 'buttonPressed' to have a true
                 value if the button is pressed.  The function is triggered by
                 ButtonInt, an External Interrupt Object
        @param IRQ_src The interrupt request from pressing the button
        @return buttonpressed = True
        
   '''    
    ## @brief Defines a global variable buttonPressed 
    #  @details buttonpressed is used to recognize when the button is pressed
    #           which is true or not pressed which is false
    #
   global buttonPressed
   buttonPressed = True

def SquareWave(t):
    '''! @brief Creates a Square Wave pattern
         @details Start off the wave at one then use a round function to have our 
                  value from the modulo function remainder go to 0 or 1
         @param t The wave changes as a function of time
         @return 0 or 1 
        
    '''
    
    return 1 - round(t%1)

def SineWave(t):
    
   '''! @brief Creates a Sine Wave pattern
        @details Uses math mod to create a sine wave which is then adjusted via 
                 various arthimetic to move the function to the desired graph
        @param t The wave changes as a function of time
        @return Any decimal or whole number between 0 and 1
        
   '''
   return math.sin(2*3.1415*t/10)*0.5 + 0.5
 
def SawWave(t):
    '''! @brief Creates a Saw Wave pattern
         @details Uses the modulo function to create all numbers between 0 and 1
         @param t The wave changes as a function of time
         @return The value of t*mod(1), which is any decimal between 0 and 1
        
    '''
    return t%1
    
    
## @brief Creates an External Interrupt Object
#  @details Configures an interrupt to run when Blue Button on Nucleo is pressed
#
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

if __name__ == '__main__':
    buttonPressed = False
    ## @brief Creates states
    #  @details Breaks all desired actions into 4 states.  Starts at state = 0.  
    #
    state = 0
    print('Welcome!  Press the blue button, B1, on the Nucleo to cycle LED patterns.')

    while True:
        try:
            #uses an infinite loop to run the following lines until the except
            #condition is met
            if state == 0:
                if buttonPressed:
                    buttonPressed = False
                    #once if statement is met, reset variable to false
                    print('Square Wave pattern selected')
                    ## @brief Creates a variable called start time
                    #  @details start_time is used to determine the time function
                    #           in our code
                    #
                    start_time = 0
                    state = 1
                    #switches to the next state
                    
            elif state == 1:
                ## @brief Creates a variable called current_time
                #  @details Current_time uses the timer that gives time in ms
                #
                current_time = time.ticks_ms()
                ## @brief Creates a variable called waveformTime
                #  @details waveformTime is current time minus start time and 
                #           converts to seconds
                #
                waveformTime = time.ticks_diff(current_time, start_time)/1000
                ## @brief Creates a variable called brt
                #  @details Creates variable to run the desired wave function 
                #           and converts 
                #
                brt = 100*SquareWave(waveformTime)
                t2ch1.pulse_width_percent(brt)
                #function uses brightness percentage to control the brightness
                #of the LED
                if buttonPressed:
                    buttonPressed = False
                    print('Sine Wave pattern selected')
                    start_time = current_time
                    #redefines start time to allow the graphs to start at
                    #desired time and location
                    state = 2
                    #switches to the next state
                    
            elif state == 2:
                current_time = time.ticks_ms()
                #creates variable for current time
                waveformTime = time.ticks_diff(current_time, start_time)/1000
                #creates variable with current time minus start time and 
                #converts to seconds
                brt = 100*SineWave(waveformTime)
                #creates variable to run the desired wave function and converts
                #to a percentage to use for brightness
                t2ch1.pulse_width_percent(brt)
                #function uses brightness percentage to control the brightness
                #of the LED
                if buttonPressed:
                    buttonPressed = False
                    print('Saw Wave pattern selected')
                    start_time = current_time
                    #redefines start time to allow the graphs to start at
                    #desired time and location
                    state = 3
                    #switches to the next state
                    
            elif state == 3:
                current_time = time.ticks_ms()
                #creates variable for current time
                waveformTime = time.ticks_diff(current_time, start_time)/1000
                #creates variable with current time minus start time and 
                #converts to seconds
                brt = 100*SawWave(waveformTime)
                #creates variable to run the desired wave function and converts
                #to a percentage to use for brightness
                t2ch1.pulse_width_percent(brt)
                #function uses brightness percentage to control the brightness
                #of the LED
                if buttonPressed:
                    buttonPressed = False
                    print('Square Wave pattern selected')
                    start_time = current_time
                    #redefines start time to allow the graphs to start at
                    #desired time and location
                    state = 1
                    #switches to the next state
            

        except KeyboardInterrupt:
            #creates a keyboardinterrupt to break infinite loop
            break
        
    print('Program Terminating')