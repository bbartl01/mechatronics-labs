'''! @page lab1page Lab1
    @tableofcontents
    Getting started with hardware.
    @section sec_intro1 Introduction
    This lab had us create three different LED patterns on a Nucleo.  
    See @ref lab01.py for detailed documentation.
'''