'''! @page lab2page Lab2
    @tableofcontents
    Incremental Encoders
    @section sec_intro2 Introduction
    This lab created a User Interface for users to interact with an Encoder.  
    The interface allowed users to determine the position of the encoder, the change 
    in position (delta), and to collect the position of the encoder over a 30 second 
    interval.  An example of such data collection is depicted below:
    @image html Lab2Data.png
    @section sec_main2 main
    This file calls all the tasks and classes necessary for the interface to work.
    See @ref main.py for more information.
    @section sec_enc2 Encoder Driver
    This file creates the encoder class.  
    Please see @ref encoder.py for details about the encoder driver.  
    Please see @ref encoder.Encoder for details about the encoder class.
    @section sec_UTask2 User Task
    This file creates the task that runs the User Interface.
    Please see @ref UTask.py for more information.
    @section sec_ETask2 Encoder Task
    This file creates the task that collects data from the encoder.  
    Please see @ref ETask.py for more information.
    @section sec_shares2 Shares Class
    This file creates the class that enables variables to be shared across multiple tasks.  
    See @ref shares.py for more information.
'''