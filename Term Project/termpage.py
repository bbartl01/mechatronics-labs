'''! @page termpage TermProject
    @tableofcontents
    Term Project
    
    @section sec_intro305FF Introduction
    In this final project, we take everything we've learned in ME 305 to try to balance 
    a ball on top of a platform.  As the reader may have noticed, each lab prior to this 
    built on the previous lab in an effort to reach this project.  For this project, the 
    new addition is a touch panel capable of sensing the ball's location on the platform.  
    Similar to the IMU, we had to create a calibration process for this touch panel as well 
    as a way to read calibration coefifients to it to avoid the need to recalibrate every time.  
    
    Please see the video below for a description of the features of our term project:
    https://drive.google.com/file/d/1O92MHnQORKZ-1yQg_3kzjiBA-JIQwFbI/view 
    
    A block diagram of our closd loop scheme is depicted below:
    @image html term_block_diagram.png width=1000px
    
    NOTE FOR GRADER: The documentation of our touch panel scan time is located in @ref touchpad.py
    
    @section sec_UI305FF User Interface
    Our video mentions the main features of our user interface.  However, to provide a 
    clearer illustration of these features, images of the User Interface are shown and 
    explained here.  
    
    Our User Interface is shown below:
    @image html UI.png width=500px
    
    @subsection subsec_Basic Basic User Interface Commands
    Several of the bsic features are shown in the picture below:
    @image html VariousCommands.png width=800px
    The first 4 lines show the execution of "p", "d", "D", and "v" respectively.  
    From there, "K" is pressed, prompting the user to enter gain values for the outer loop.  
    The user is first prompted to enter a value for Kp, and after the user inpts a value, is 
    immediately prompted for a Kd value, followed by a Ki value.  Below that is a similar 
    response for hitting "k" for inner loop gains.  
    
    @subsection subsec_Data Data Collection
    Shown below is a snippet of our data collection interface:
    @image html DataCollect.png width=700px
    The user can collect data for 15 seconds by hitting "c" or "C" and can exit prematurely by hitting 
    "s" or "S" (as is depicted in the image).  The collectd data is then written to a CSV file.  When put 
    into a MATLAB code, the following plots are generated (the data for these specific plots was obtained during the video):
    @image html YvX.png width=500px
    @image html XvT.png width=500px
    @image html YvT.png width=500px
    @image html VXvT.png width=500px
    @image html VYvT.png width=500px
    @image html THXvT.png width=500px
    @image html THYvT.png width=500px
    @image html OMXvT.png width=500px
    @image html OMYvT.png width=500px
    
    @subsection subsec_DirectAngle Direct Angle Control
    We also added the ability for the user to select the angle of the platform.  
    Below depicts a segment of a user controlling the angle from motor 1:
    @image html DirectAngle.png width=800px
    First, the user must press "w" to enable closed loop control.  Then the user presses "y" 
    to signify that they want to control the angle from motor 1.  This prints a message about what angles 
    are acceptable.  After entering an angle, they are reminded that the must enter gain values by pressing 
    "k".  After the user is satisfied with the program, they can hit "q" to exit this interface.  
    
    @section sec_files305FF Files
    
    @subsection subsec_main305FF main
    This file calls all the tasks and classes necessary for the interface to work.
    See @ref main.py for more information.  Task diagram is depicted below:
    @image html term_task_diagram.png width=500px
    
    @subsection subsec_UTask305FF User Task
    This file creates the task that runs the User Interface.
    Please see @ref UTask5.py for more information.  State transition diagram is 
    depicted below:
    @image html term_ui_task_std.png width=500px
    
    @subsection subsec_TTask305FF Touchpanel Task
    This file creates the task that runs the touchpanel.
    Please see @ref TTask.py for more information.  State transition diagram is 
    depicted below:
    @image html ttask_std.png width=500px
    
    @subsection subsec_IMUTask305FF IMU Task
    This file creates the task that runs the IMU.
    Please see @ref IMUTask.py for more information.  State transition diagram is 
    depicted below:
    @image html imutask_std.png width=500px
    
    @subsection subsec_CTask5305FF Conroller Task
    This file creates the task that performs closed loop control on a motor.  
    Please see @ref CTask5.py for more information.  State transition diagram is 
    depicted below:
    @image html term_ctask_std.png width=500px
    
    @subsection subsec_MTask5305FF Motor Task
    This file creates the task that changes the duty cycles applied to the motors.  
    Please see @ref MTask.py for more information.  State transition diagram is 
    depicted below:
    @image html MTaskSTD3.png width=500px
    
    @subsection subsec_touch305FF Touchpad Driver
    This file creates the driver for the touchpad. 
    Please see @ref touchpad.Touchpad for details about this class.  
    Please see @ref touchpad.py for details about the file.
    
    @subsection subsec_BNO305FF BNO055 Driver
    This file creates the driver for the BNO055. 
    Please see @ref BNO055.BNO055 for details about this class.  
    Please see @ref BNO055.py for details about the file.
    
    @subsection subsec_CLC305FF Closed Loop Controller Class
    This file creates the class that enables closed loop control.  
    Please see @ref closedloop5.ClosedLoop for details about this class.  
    Please see @ref closedloop5.py for details about the file.
    
    @subsection subsec_mot305FF motor Driver
    This file creates the motor class.  
    Please see @ref motor5.Motor for details about the motor class.
    
    @subsection subsec_shares305FF Shares Class
    This file creates the class that enables variables to be shared across multiple tasks.  
    See @ref shares.py for more information.
'''